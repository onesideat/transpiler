# Transpiler

Transpile and Minify SCSS with SJS code to production on the fly or using CLI

**Installation with composer.json**
~~~json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/onesideat/transpiler.git"
        }
    ],
    "require": {
        "oneside/transpiler": "dev-master"
    }
}
~~~

## Test environment

**Add to .htaccess**
~~~
RewriteEngine On
RewriteRule ^assets\/(css|js)\/(.*?)\.(css|js)$ transpiler.php?file=$1 [L,QSA,NC]
~~~
or **nginx.conf**
~~~
location ~* assets\/(?:css|js)\/(.*?)\.(css|js)$ {
    root      html;
    try_files $uri $uri/ /transpiler.php;
}
~~~

**Create transpile.php in public**
~~~php
require dirname(__DIR__).'/vendor/autoload.php';

define('SRC_PATH', dirname(__DIR__) . '/assets');
define('DIST_PATH', dirname(__DIR__) . '/public');

new \Oneside\Transpiler([
    'mode' => isset($_SERVER['REQUEST_URI']) ? 'test' : 'live',

    'paths' => [
        'src' => SRC_PATH,
        'dist' => DIST_PATH,
        'css' => DIST_PATH . '/assets/css/',
        'scss' => SRC_PATH . '/scss/',
        'js' => DIST_PATH . '/assets/js/',
        'sjs' => SRC_PATH . '/sjs/'
    ],

    'js_transpiler' => false //nodejs is required
]);
~~~

**Node.js packages** *(required for js_transpiler option only)*
~~~
cd /path/to/project/
npm install
~~~

## Production environment

**Compile files to production**
~~~
cd /path/to/project/
php ./public/transpiler.php
~~~
